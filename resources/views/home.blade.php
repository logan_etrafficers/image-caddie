@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Your API Key:
                    {{ Auth::user()->api_key }}
                    
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    Upload a New Image
                </div>
                <div class="card-body">
                    <image-uploader api-token="{{ Auth::user()->api_token }}"></image-uploader>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div style="display: grid; grid-template-columns: repeat(4, 1fr)">
                        @foreach (Auth::user()->images as $image)
                        <a href="/api/images/{{ $image->id }}" target="_blank">
                            <img src="/images/{{ $image->id }}?fit_x=200&fix_y=200" alt="Image preview: {{ $image }}" />
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
