<?php

namespace App\Http\Controllers;

use App\Image;
use App\User;
use Auth;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as ImageFacade;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return $user->images;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $token = $request->bearerToken();
        if ($token == null) {
            $token = $request->get('api_token');
        }

        $user = User::where('api_token', $token)->firstOrFail();
        $name = $user->id . "_" . time() . '.' . $image->extension();
        $path = 'user_images/' . $user->id;
        $imageData = ImageFacade::make($image);
        $imageData->orientate();
        $imageData->save(storage_path('app/' . $path . '/' . $name));
        // $image->storeAs($path, $name);
        $image = new Image;
        $image->user_id = $user->id;
        $image->file_path = $path . '/' . $name;
        $image->save();
        
        return $image;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Image $image)
    {
        $modifiers = [
            'image_id' => $image->id,
            'width' => $request->get('w'),
            'height' => $request->get('h'),
            'crop_width' => $request->get('cw'),
            'crop_height' => $request->get('ch'),
            'crop_x' => $request->get('cx'),
            'crop_y' => $request->get('cy'),
            'encode' => $request->get('encode'),
            'fit_x' => $request->get('fit_x'),
            'fit_y' => $request->get('fit_y'),
        ];

        $hash = hash('crc32', json_encode($modifiers));

        return Cache::rememberForever($hash, function () use ($image, $modifiers) {
            $path = Storage::get($image->file_path);
            $imageData = ImageFacade::make($path);

            // Resize
            if (isset($modifiers['width'])) {
                $imageData->widen($modifiers['width']);
            }
            if (isset($modifiers['height'])) {
                $imageData->heighten($modifiers['height']);
            }

            // Fit
            $fitX = $modifiers['fit_x'];
            $fitY = $modifiers['fit_y'];
            if ($fitX != null){
                $imageData->fit($fitX, $fitY);
            }

            // Crop
            $cropWidth = $modifiers['crop_width'];
            $cropHeight = $modifiers['crop_height'];
            $cropX = $modifiers['crop_x'];
            $cropY = $modifiers['crop_y'];
            if ($cropWidth != null && $cropHeight != null) {
                $cropCoords = null;
                if ($cropX != null && $cropY != null) {
                    $imageData->crop($cropWidth, $cropHeight, $cropX, $cropY);
                } else {
                    $imageData->crop($cropWidth, $cropHeight);
                }
            }

            // Encode
            if (isset($modifiers['encode'])) {
                $imageData->encode($modifiers['encode']);
            }

            return $imageData->response();
        });
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
