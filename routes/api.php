<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('images', 'ImageController');
});


Route::get('images', 'ImageController@index')
    ->name('images.index')->middleware('auth:api');
Route::post('images', 'ImageController@store')
    ->name('images.store')->middleware('auth:api');
